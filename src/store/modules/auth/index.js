/**
 * Auth Module
 */
import Vue from "vue";
import Nprogress from "nprogress";
import router from "../../../router";
import apiAuth from "../../../api/auth";

const state = {
  city: "",
  restaurant: "",
  user: JSON.parse(localStorage.getItem("user")),
  isUserSigninWithAuth0: Boolean(localStorage.getItem("isUserSigninWithAuth0")),
};

// getters
const getters = {
  getUser: (state) => {
    return state.user;
  },
  isUserSigninWithAuth0: (state) => {
    return state.isUserSigninWithAuth0;
  },
  actualCity: (state) => {
    return state.city;
  },
  actualRestaurant: (state) => {
    return state.restaurant;
  },
};

// actions
const actions = {
  signinUserInMesa(context, payload) {
    const { user } = payload;
    context.commit("loginUser");

    apiAuth
      .login(user)
      .then((response) => {
        Nprogress.done();
        var user = response.data.user;
        localStorage.setItem("access_token", response.data.access_token);
        context.commit("loginUserSuccess", user);
      })
      .catch((error) => {
        Nprogress.done();
        if (error.response.status === 403) {
          context.commit("loginUserFailure", error.response.data);
        } else {
          context.commit("loginUserFailure", error);
        }
      });
  },
  logoutUserFromFirebase(context) {
    Nprogress.start();
    context.commit("logoutUser");
  },
  signInUserWithAuth0(context, payload) {
    context.commit("signInUserWithAuth0Success", payload);
  },
  signOutUserFromAuth0(context) {
    context.commit("signOutUserFromAuth0Success");
  },
  async setActualCity({ commit }) {
    try {
      commit("setActualCityMut", {
        city: sessionStorage.getItem("cityActive"),
      });
    } catch (e) {
      console.log("error");
    }
  },
  async setActualRestaurant({ commit }) {
    try {
      commit("setActualRestaurantMut", {
        restaurant: sessionStorage.getItem("restaurantActive"),
      });
    } catch (e) {
      console.log("error");
    }
  },
  async setFirstRestaurant({ commit }) {
    try {
      sessionStorage.setItem(
        "restaurantActive",
        sessionStorage.getItem("firstRestaurant")
      );

      commit("setActualRestaurantMut", {
        restaurant: sessionStorage.getItem("firstRestaurant"),
      });
    } catch (e) {
      console.log("error");
    }
  },
};

// mutations
const mutations = {
  loginUser() {
    Nprogress.start();
  },
  loginUserSuccess(state, user) {
    state.user = user;
    localStorage.setItem("user", JSON.stringify(user));
    state.isUserSigninWithAuth0 = false;
    router.push("/dashboard");
    setTimeout(function() {
      Vue.notify({
        group: "loggedIn",
        type: "success",
        text: "User Logged In Success!",
      });
    }, 1500);
  },
  loginUserFailure(state, error) {
    Nprogress.done();
    Vue.notify({
      group: "loggedIn",
      type: "error",
      text: error.message,
    });
  },
  logoutUser(state) {
    state.user = null;
    state.city = "";
    state.restaurant = "";
    localStorage.removeItem("user");
    localStorage.removeItem("cityActive");
    localStorage.removeItem("restaurantActive");
    localStorage.removeItem("access_token");
    router.push("/session/login");
  },
  signUpUser() {
    Nprogress.start();
  },
  signUpUserSuccess(state, user) {
    state.user = localStorage.setItem("user", user);
    router.push("/dashboard");
    Vue.notify({
      group: "loggedIn",
      type: "success",
      text: "Account Created!",
    });
  },
  signUpUserFailure(state, error) {
    Nprogress.done();
    Vue.notify({
      group: "loggedIn",
      type: "error",
      text: error.message,
    });
  },
  signInUserWithAuth0Success(state, user) {
    state.user = user;
    localStorage.setItem("user", JSON.stringify(user));
    state.isUserSigninWithAuth0 = true;
  },
  signOutUserFromAuth0Success(state) {
    state.user = null;
    localStorage.removeItem("user");
    localStorage.removeItem("cityActive");
    localStorage.removeItem("restaurantActive");
  },
  setActualCityMut(state, payload) {
    state.city = payload.city;
  },
  setActualRestaurantMut(state, payload) {
    state.restaurant = payload.restaurant;
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
