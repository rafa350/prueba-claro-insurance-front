// src/main.js

import "babel-polyfill";
import Vue from "vue";
import App from "./App.vue";
import vuetify from "./plugins/vuetify";
import * as VueGoogleMaps from "vue2-google-maps-withscopedautocomp";
import VueBreadcrumbs from "vue2-breadcrumbs";
import VueResource from "vue-resource";
import Notifications from "vue-notification";
import velocity from "velocity-animate";
import Nprogress from "nprogress";
import VueI18n from "vue-i18n";
import VueTour from "vue-tour";
import fullscreen from "vue-fullscreen";
import VueQuillEditor from "vue-quill-editor";

// global components
import GlobalComponents from "./globalComponents";
// router
import router from "./router";

// store
import { store } from "./store/store";

// include all css files
import "./lib/VuelyCss";

// messages
import messages from "./lang";

Vue.config.productionTip = false;
import VuePlayer from "@iomariani/vue-player";

Vue.component("vue-player", VuePlayer);
//funcion para formatear numeros
Vue.prototype.$f = function(value) {
  if (value) {
    var locale = "de";
    var options = { minimumFractionDigits: 0, maximumFractionDigits: 2 };
    var formatter = new Intl.NumberFormat(locale, options);
    var result = parseFloat(value);
    return formatter.format(result);
  }
  return value;
};
//funcion para verificar si es merchant
Vue.prototype.$merchant = function(value) {
  if (value) {
    if (value.includes("Merchant")) {
      return true;
    }
  }
  return false;
};

// navigation guards before each
router.beforeEach((to, from, next) => {
  Nprogress.start();
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (localStorage.getItem("user") === null) {
      next({
        path: "/session/login",
        query: { redirect: to.fullPath },
      });
    } else {
      next();
    }
  } else {
    next(); // make sure to always call next()!
  }
});

// navigation guard after each
router.afterEach(() => {
  Nprogress.done();
  setTimeout(() => {
    const contentWrapper = document.querySelector(".v-content__wrap");
    if (contentWrapper !== null) {
      contentWrapper.scrollTop = 0;
    }
    const boxedLayout = document.querySelector(
      ".app-boxed-layout .app-content"
    );
    if (boxedLayout !== null) {
      boxedLayout.scrollTop = 0;
    }
    const miniLayout = document.querySelector(".app-mini-layout .app-content");
    if (miniLayout !== null) {
      miniLayout.scrollTop = 0;
    }
  }, 200);
});
import PortalVue from "portal-vue";
Vue.use(PortalVue);

import moment from "moment";

Vue.filter("formatDate", function(value) {
  if (value) {
    return moment(String(value)).format("hh:mm");
  }
});
//form wizard
import VueFormWizard from "vue-form-wizard";
import "vue-form-wizard/dist/vue-form-wizard.min.css";
Vue.use(VueQuillEditor);
import DaySpan from "dayspan-vuetify-2";
import "material-design-icons-iconfont/dist/material-design-icons.css";

Vue.use(DaySpan, {
  methods: {
    getDefaultEventColor: () => "#1976d2",
  },
});

Vue.use(VueFormWizard);
Vue.use(VueI18n);
Vue.use(VueTour);
Vue.use(VueResource);
Vue.use(VueBreadcrumbs);
Vue.use(Notifications, { velocity });
Vue.use(fullscreen);
Vue.use(GlobalComponents);
Vue.use(VueGoogleMaps, {
  load: {
    libraries: "places", // This is required if you use the Autocomplete plugin
    key: "AIzaSyCAUhMIdGkR1KruQw-AHMOGrqmc_K7KmnA", // Add your here your google map api key
  },
});

const i18n = new VueI18n({
  locale: store.getters.selectedLocale.locale, // set locale
  messages, // set locale messages
});

new Vue({
  store,
  i18n,
  router,
  vuetify,
  render: (h) => h(App),
  components: { App },
}).$mount("#app");
