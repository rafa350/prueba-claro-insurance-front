import en from "./en";
import es from "./sp";

export default {
  en: {
    message: en,
  },
  es: {
    message: es,
  },
};
