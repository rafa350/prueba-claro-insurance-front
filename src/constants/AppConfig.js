/**
 * App Config File
 */
export default {
  appLogo:
    "https://ci3.googleusercontent.com/proxy/7xgFSUUPLl9mpJkJyWzKN0F3AYgoq-sL1NydIBrzMycd8KDQdBwAn7ju6AOstgvFneHKG1MuCWK5AZ9ss1XySJPs2LdOxxa9LVFlsshy2ZpOXKa7SJr4V6CK_sT1p4l23u6bfG4zIhWHPEvjqg6Q8Q=s0-d-e1-ft#https://landing-pages.claroinsurance.com/hubfs/firma-email-claroinsurance/Firma-correos-9.png", // App Logo,
  darkLogo: "/static/img/site-dark-logo.png", // dark logo
  appLogo2: "/static/img/logo_mesa_n.png", // App Logo 2 For Login & Signup Page
  marker: "/static/img/pin.png", // App Logo 2 For Login & Signup Page
  markerU: "/static/img/pin-u.png", // App Logo 2 For Login & Signup Page
  markerC1: "/static/img/pin-c1.png", // App Logo 2 For Login & Signup Page
  markerC2: "/static/img/pin-c2.png", // App Logo 2 For Login & Signup Page
  markerC3: "/static/img/pin-c3.png", // App Logo 2 For Login & Signup Page
  brand: "Objetos de Aprendizaje", // Brand Name
  copyrightText: "Vuely © 2020 All Rights Reserved.", // Copyright Text
  enableUserTour: process.env.NODE_ENV === "production" ? true : false, // Enable User Tour
  weatherApiId: "b1b15e88fa797225412429c1c50c122a1", // weather API Id
  weatherApiKey: "69b72ed255ce5efad910bd946685883a", // weather APi key
};
