import axios from "axios";
import { Service } from "axios-middleware";
import Vue from "vue";

const token = localStorage.getItem("access_token");
axios.defaults.headers.common["Accept"] = "application/json"; // for POST requests
console.log(`Bearer ${token}`);
if (token) {
  axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
} else {
  axios.defaults.headers.common["Authorization"] = null;
}

const service = new Service(axios);

service.register({
  onResponse(response) {
    if (response.status == 403) {
      Vue.notify({
        group: "loggedIn",
        type: "error",
        text: response.message,
      });
    }
    return response;
  },
});

console.log("Ready to fetch.");

export default axios.create({
  baseURL: process.env.VUE_APP_URL,
  //   aseURL: "http://ec2-18-228-213-193.sa-east-1.compute.amazonaws.com/api/v1/",
});
