import api from "Api";

export default {
  async getCountries(params) {
    return api.get(`/get_countries`, { params });
  },

  async getStates(id) {
    return api.get(`/get_states/${id}`);
  },
  async getCities(id) {
    return api.get(`/get_cities/${id}`);
  },
  async getParents(id) {
    return api.get(`/get_parents_from_city/${id}`);
  },
};
