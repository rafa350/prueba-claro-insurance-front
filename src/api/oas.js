import api from "Api";
const token = localStorage.getItem("access_token");

export default {
  async fetch(params) {
    if (localStorage.getItem("access_token")) {
      console.log(`Bearer ${token}`);

      api.defaults.headers.common = {
        Authorization: `Bearer ${token}`,
        Accept: "application/json",
      };
      console.log("aqui");
    }
    return api.get(`/oaa`, { params });
  },
  async trasnformOa(oa, params) {
    return api.get(`/oaa/${oa}`, { params });
  },
};
