import { store } from "./../../store/store";

export function permission() {
  const user = store.getters.getUser;

  if (user.is_admin == 1) {
    return false;
  }
  return true;
}
