import api from "Api";

export default {
  async login(params) {
    return api.post("/auth/login", params);
  },
};
