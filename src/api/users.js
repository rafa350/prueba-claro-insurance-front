import api from "Api";

export default {
  async fetch(params) {
    return api.get(`/users`, { params });
  },
  async create(params) {
    return api.post(`/users`, params);
  },
  async update(id, params) {
    return api.put(`/users/${id}`, params);
  },
  async delete(id) {
    return api.delete(`/users/${id}`);
  },
};
