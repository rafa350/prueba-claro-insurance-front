export default class User {
  constructor(user) {
    if (user) {
      const {
        id = null,
        name = null,
        email = null,
        phone = "(+569)",
        password = null,
        city_id = null,
      } = user;
      this.id = id;
      this.name = name;
      this.phone = phone;
      this.email = email;
      this.password = password;
      this.city_id = city_id;
    } else {
      this.id = null;
      this.name = null;
      this.phone = null;
      this.email = null;
      this.password = null;
      this.city_id = null;
    }
  }
}
