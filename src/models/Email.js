export default class User {
  constructor(email) {
    if (email) {
      const {
        id = null,
        destinatary = null,
        subject = null,
        content = null,
      } = email;
      this.id = id;
      this.destinatary = destinatary;
      this.subject = subject;
      this.content = content;
    } else {
      this.id = null;
      this.destinatary = null;
      this.subject = null;
      this.content = null;
    }
  }
}
