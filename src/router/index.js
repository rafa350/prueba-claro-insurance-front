import Vue from "vue";
import Router from "vue-router";

//routes
import defaultRoutes from "./default";

// session components

const LoginOne = () => import("Views/session/LoginOne");

const Auth0CallBack = () => import("Components/Auth0Callback/Auth0Callback");

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    defaultRoutes,
    {
      path: "/callback",
      component: Auth0CallBack,
    },
    {
      path: "/session/login",
      component: LoginOne,
      meta: {
        title: "message.login",
        breadcrumb: null,
      },
    },
  ],
});
