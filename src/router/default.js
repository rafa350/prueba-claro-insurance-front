import Full from "Container/Full";

// dashboard components
const Ecommerce = () => import("Views/dashboard/Ecommerce");
const oaResult = () => import("Views/dashboard/OaResult");
// users views
const Users = () => import("Views/users/TableUsers");
// users views

export default {
  path: "/",
  component: Full,
  redirect: "/dashboard",
  children: [
    {
      path: "/dashboard",
      component: Ecommerce,
      meta: {
        requiresAuth: true,
        title: "message.ecommerce",
        breadcrumb: null,
      },
    },
    {
      path: "/oaa",
      component: oaResult,
      meta: {
        requiresAuth: true,
        title: "message.ecommerce",
        breadcrumb: null,
      },
    },
    {
      path: "/users",
      component: Users,
      meta: {
        requiresAuth: true,
        title: "message.Users",
        breadcrumb: "message.Users",
      },
    },
  ],
};
