// Validations
export const rules = {
  required: (v) => !!v || "Este campo es requerido",
  email: (v) =>
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
      v
    ) || "Email inválido.",
  password: (v) =>
    (v && v.length >= 8) || "La contraseña debe incluir al menos 8 caracteres",
  length11: (v) =>
    (v && v.length <= 11) || "La cedula debe incluir maximo 11 caracteres",
  length100: (v) =>
    (v && v.length <= 100) || "Nombre debe incluir maximo 100 caracteres",
  length10: (v) =>
    v.length <= 10 || "Telefono debe incluir maximo 10 caracteres",
  cantidadEgreso: (v) => (v && v >= 1) || "La cantidad mínima es 1",
  precioEgreso: (v) => (v && v >= 1) || "Precio no puede ser 0",
  letters: (v) =>
    (v &&
      /^[a-zA-ZÀ-ÿ\u00f1\u00d1]+(\s*[a-zA-ZÀ-ÿ\u00f1\u00d1]*)*[a-zA-ZÀ-ÿ\u00f1\u00d1]+$/.test(
        v
      )) ||
    "Solo debe contener letras",

  strongPassword: (v) =>
    (v &&
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])([A-Za-z\d$@$!%*?&]|[^ ]){8,15}$/.test(
        v
      )) ||
    "Debe contener al menos 8 caracteres,una letra mayúscula,una letra mayúscula, un dígito, 1 caracter especial",
};

export function formatPrice(value) {
  let val;
  if (value === "0.00" || value === "0" || value === 0) {
    return 0;
  } else if (value / 1 < 1) {
    val = (value / 1).toFixed(2).replace(".", ",");
  } else {
    val = (value / 1).toFixed(0).replace(".", ",");
  }
  return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}
